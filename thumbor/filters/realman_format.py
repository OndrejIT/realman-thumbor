#!/usr/bin/python
# -*- coding: utf-8 -*-

# thumbor imaging service
# https://github.com/thumbor/thumbor/wiki

# Licensed under the MIT license:
# http://www.opensource.org/licenses/mit-license
# Copyright (c) 2011 globo.com timehome@corp.globo.com
#
# Author Ondrej Barta
# ondrej@ondrej.it
# Copyright 2015

from thumbor.filters import BaseFilter, filter_method
from thumbor.utils import logger


ALLOWED_FORMATS = ["png", "jpeg", "jpg", "gif", "webp"]


class Filter(BaseFilter):

    @filter_method(BaseFilter.String)
    def realman_format(self, format):
        # V pripade RGBA (alfa kanalu) obrazek sloucime s bilim obrazkem
        if self.engine.image.mode == "RGBA":
            width, height = self.engine.size
            logger.debug("[REALMAN FORMAT] png to: %s" % format.lower())

            self.format_engine = self.engine.__class__(self.context)
            self.format_engine.image = self.format_engine.gen_image((width, height), "white")
            self.format_engine.paste(self.engine, (0, 0), merge=True)
            self.engine.image = self.format_engine.image

            self.context.request.format = format.lower()

        elif format.lower() not in ALLOWED_FORMATS:
            logger.warn("[REALMAN FORMAT] Format not allowed: %s" % format.lower())
            self.context.request.format = None
        else:
            logger.debug("[REALMAN FORMAT] Format specified: %s" % format.lower())
            self.context.request.format = format.lower()
