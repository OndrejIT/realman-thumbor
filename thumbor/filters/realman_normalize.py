#!/usr/bin/python
# -*- coding: utf-8 -*-

# Author Ondrej Barta
# ondrej@ondrej.it
# Copyright 2015
#
# Normalizace obrazku (stran)

from thumbor.filters import BaseFilter, filter_method


class Filter(BaseFilter):

    @filter_method(BaseFilter.Number)
    def realman_normalize(self, value):
        width, height = self.engine.size
        # Pokud je sirka a vyska mensi nez value, nedovolime zvetsit
        if width <= value and height <= value:
            pass
        elif width > height:
            height = self.engine.get_proportional_height(value)
            width = self.engine.get_proportional_width(height)
        else:
            width = self.engine.get_proportional_width(value)
            height = self.engine.get_proportional_height(width)
        
        self.engine.resize(width, height)
