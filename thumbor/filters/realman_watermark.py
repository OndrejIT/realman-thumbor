#!/usr/bin/python
# -*- coding: utf-8 -*-

# Author Ondrej Barta
# ondrej@ondrej.it
# Copyright 2015

from os.path import splitext
from thumbor.filters import BaseFilter, filter_method
from thumbor.loaders import LoaderResult

from thumbor.utils import logger


class Filter(BaseFilter):

    def paste_watermark(self, buffer):
        self.watermark_engine.load(buffer, self.extension)

        image_w, image_h = self.engine.size # Sirka Vyska obrazku
        border = 12 # Ramecek fotky

        watermark_w = self.percent * image_w / 100
        # Pokud je watermark moc sirokej, nedovolime ho roztahnout vic nez je zadouci
        border_2 = border * 2
        if watermark_w > image_w - border_2:
            watermark_w = image_w - border_2
        # Vyska vodoznaku v zachovani pomeru
        watermark_h = self.watermark_engine.get_proportional_height(watermark_w)

        # Fix pro vejsku SC
        if "sc" == self.position.lower() and watermark_h > image_h - (image_h / 1.5) - border:
            watermark_h =  image_h - (image_h / 1.5) - border
            watermark_w = self.watermark_engine.get_proportional_width(watermark_h)

        # Pokud je watermark moc vysokej, nedovolime ho roztahnout vic nez je zadouci
        if watermark_h > image_h - border_2:
            watermark_h = image_h - border_2
            watermark_w = self.watermark_engine.get_proportional_width(watermark_h)

        self.watermark_engine.resize(watermark_w, watermark_h)

        middle_w = (image_w / 2) - (watermark_w / 2)
        middle_h = (image_h / 2) - (watermark_h / 2)

        left = border
        right = image_w - watermark_w - border
        top = border
        down = image_h - watermark_h - border

        position = self.position.lower().split("-")

        # Nahore vlevo
        if "tl" in position or "all" in position:
            self.engine.paste(self.watermark_engine, (left, top), merge=True)
        # Nahore uprostred
        if "tc" in position or "all" in position:
            self.engine.paste(self.watermark_engine, (middle_w, top), merge=True)
        # Nahore vpravo
        if "tr" in position or "all" in position:
            self.engine.paste(self.watermark_engine, (right, top), merge=True)
        # Uprostred vlevo
        if "cl" in position or "all" in position:
            self.engine.paste(self.watermark_engine, (left, middle_h), merge=True)
        # Stred
        if "cc" in position or "all" in position:
            self.engine.paste(self.watermark_engine, (middle_w, middle_h), merge=True)
        # Uprostred vpravo
        if "cr" in position or "all" in position:
            self.engine.paste(self.watermark_engine, (right, middle_h), merge=True)
        # Spodni stred - tretina fotky
        if "sc" == self.position.lower():
            self.engine.paste(self.watermark_engine, (middle_w, image_h / 1.5), merge=True)
        # Dole vlevo
        if "bl" in position or "all" in position:
            self.engine.paste(self.watermark_engine, (left, down), merge=True)
        # Dole uprostred
        if "bc" in position or "all" in position:
            self.engine.paste(self.watermark_engine, (middle_w, down), merge=True)
        # Dole vpravo
        if "br" in position or "all" in position:
            self.engine.paste(self.watermark_engine, (right, down), merge=True)
        # Opakovane prez fotku
        if self.position.lower() == "rp" or self.position.lower() == "rp2":
            counter_h = 0
            while True:
                position_h = watermark_h * counter_h

                counter_w = 0
                while True:
                    if self.position.lower() == "rp":
                        position_w = (watermark_w * counter_w) - (watermark_w / 3) * counter_h
                    else:
                        position_w = watermark_w * counter_w
                    counter_w += 1
                    self.engine.paste(self.watermark_engine, (position_w, position_h), merge=True)
                    if position_w >= image_w:
                        break

                if position_h >= image_h:
                    break
                counter_h += 1

        logger.debug("[REALMAN WATERMARK] %s %s %s" % (self.url, self.position, str(self.percent)))

        self.callback()

    def load_watermark(self, result):
        # TODO if result.successful is False how can the error be handled?
        if isinstance(result, LoaderResult):
            buffer = result.buffer
        else:
            buffer = result

        try:
            self.watermark_engine.load(buffer, self.extension)
            self.paste_watermark(buffer)
        except IOError:
            logger.warn("[REALMAN WATERMARK] Watermark Not Found: " + self.url)
            self.callback()

    @filter_method(BaseFilter.String, BaseFilter.String, r"[1-9]+\d+", async=True)
    def realman_watermark(self, callback, url, position, percent):
        self.callback = callback
        self.url = url
        self.position = position
        self.percent = int(percent)
        self.extension = splitext(self.url)[-1].lower()
        self.watermark_engine = self.context.modules.engine.__class__(self.context)
        self.context.modules.loader.load(self.context, self.url, self.load_watermark)
