#!/usr/bin/python
# -*- coding: utf-8 -*-

# Author Ondrej Barta
# ondrej@ondrej.it
# Copyright 2015
#
# Normalizace obrazku (aby se vesel do rozmeru)

from thumbor.filters import BaseFilter, filter_method


class Filter(BaseFilter):

    @filter_method(r"[1-9]+\d+x[1-9]+\d+")
    def realman_wh_normalize(self, value):
        hw_width = int(value.split("x")[0])
        hw_height = int(value.split("x")[1])
        width, height = self.engine.size

        if width > hw_width:
            height = self.engine.get_proportional_height(hw_width)
            width = self.engine.get_proportional_width(height)
        if height > hw_height:
            width = self.engine.get_proportional_width(hw_height)
            height = self.engine.get_proportional_height(width)
        
        self.engine.resize(width, height)
