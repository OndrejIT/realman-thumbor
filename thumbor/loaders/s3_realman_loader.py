#!/usr/bin/python
# -*- coding: utf-8 -*-

# Author Ondrej Barta
# ondrej@ondrej.it
# Copyright 2015

from . import LoaderResult
from tornado.concurrent import return_future
from thumbor.loaders import http_loader
from thumbor.utils import logger

import urllib
import boto3
from botocore.utils import fix_s3_host
import sys
import traceback

@return_future
def load(context, url, callback):
	url = urllib.unquote(url).decode("utf8")
	result = LoaderResult()
	# Pokud neni s3:// nacitame z http://
	if url.startswith("s3://"):
		try:
			bucket, path = url.lstrip("s3://").split("/", 1)
			s3 = boto3.resource(
					"s3",
					aws_access_key_id=context.config.CEPH_ACCES_KEY,
					aws_secret_access_key=context.config.CEPH_SECRET_KEY,
					endpoint_url=context.config.CEPH_URL,
				)
			s3.meta.client.meta.events.unregister("before-sign.s3", fix_s3_host)

			obj = s3.Object(bucket_name=bucket, key=path)
			result.successful = True
			result.buffer = obj.get()["Body"].read()
		except:
			# Pokud selze, pokusime se nacist z amazonu
			e = sys.exc_info()[0]
			logger.error(traceback.format_exc(e))
			try:
				s3 = boto3.resource(
						"s3",
						aws_access_key_id=context.config.AMAZON_ACCES_KEY,
						aws_secret_access_key=context.config.AMAZON_SECRET_KEY,
						region_name=context.config.AMAZON_REGION,
					)

				obj = s3.Object(bucket_name=context.config.AMAZON_NORMALIZED_BUCKET, key=path)
				result.successful = True
				result.buffer = obj.get()["Body"].read()
			except:
				e = sys.exc_info()[0]
				logger.error(traceback.format_exc(e))
				result.error = LoaderResult.ERROR_NOT_FOUND
				result.successful = False

		callback(result)

	else:
		return http_loader.load_sync(context, url, callback, normalize_url_func=http_loader._normalize_url)
