#!/usr/bin/python
# -*- coding: utf-8 -*-

# Author Ondrej Barta
# ondrej@ondrej.it
# Copyright 2015

from thumbor.result_storages import BaseStorage
from thumbor.utils import logger

import os


class Storage(BaseStorage):
    def put(self, bytes):
        # Pokud nejsou parametry, nebo adresa, nedovolime ukladat
        if self.context.request.url == "/" or not self.context.request.arguments or not ".jpg" in self.context.request.url:
            return
        # Nedovolime ukladat pokud je storage false nebo normalizujeme vodoznak
        if "storage" in self.context.request.arguments and self.context.request.arguments["storage"] == "false" or "wmn" in self.context.request.arguments:
            return

        file_abspath = os.path.join(self.context.config.REALMAN_STORAGE, self.context.request.url.strip("/"))
        file_dir_abspath = os.path.dirname(file_abspath)
        logger.debug("[RESULT_STORAGE] putting at %s (%s)" % (file_abspath, file_dir_abspath))

        self.ensure_dir(file_dir_abspath)

        with open(file_abspath, 'w') as a_file:
            a_file.write(bytes)

    def get(self):
        pass

    def last_updated(self):
        pass
