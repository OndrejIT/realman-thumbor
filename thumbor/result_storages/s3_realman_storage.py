#!/usr/bin/python
# -*- coding: utf-8 -*-

# Author Ondrej Barta
# ondrej@ondrej.it
# Copyright 2015

from thumbor.result_storages import BaseStorage
from thumbor.utils import logger


import boto3
from botocore.utils import fix_s3_host
import sys
import traceback

class Storage(BaseStorage):
	def ceph_s3(self, bucket, path, bytes, ACL="public-read"):
		try:
			log_info = "%s/%s/%s" % (self.context.config.CEPH_URL, bucket, path)
			s3 = boto3.resource(
					"s3",
					aws_access_key_id=self.context.config.CEPH_ACCES_KEY,
					aws_secret_access_key=self.context.config.CEPH_SECRET_KEY,
					endpoint_url=self.context.config.CEPH_URL,
				)

			s3.meta.client.meta.events.unregister("before-sign.s3", fix_s3_host)
			s3.Object(bucket, path).put(
				Body=bytes,
				ACL=ACL,
				ContentType="image/{0}".format(path.split(".")[-1]),
			)

			logger.info("[RESULT_STORAGE] putting at " + log_info)
		except:
			logger.error("[RESULT_STORAGE] putting at " + log_info)
			e = sys.exc_info()[0]
			logger.error(traceback.format_exc(e))

	def amazon_s3(self, bucket, path, bytes, ACL="private"):
		try:
			log_info = "%s/%s/%s" % (self.context.config.AMAZON_LOCATION, bucket, path)
			s3 = boto3.resource(
					"s3",
					aws_access_key_id=self.context.config.AMAZON_ACCES_KEY,
					aws_secret_access_key=self.context.config.AMAZON_SECRET_KEY,
					region_name=self.context.config.AMAZON_REGION,
				)

			s3.Object(bucket, path).put(
				Body=bytes,
				ACL=ACL,
				StorageClass="STANDARD_IA",
				ContentType="image/{0}".format(path.split(".")[-1]),
			)

			logger.info("[RESULT_STORAGE] putting at " + log_info)
		except:
			logger.error("[RESULT_STORAGE] putting at " + log_info)
			e = sys.exc_info()[0]
			logger.error(traceback.format_exc(e))

	def put(self, bytes):
		url = self.context.request.url.decode("utf8").lstrip("/")
		# Nedovolime ukladat pokud je storage false
		if "storage" in self.context.request.arguments \
		and self.context.request.arguments["storage"] == "false":
			return

		# Pokud je normalizace, ukladame do predem definovaneho kbeliku
		if "n" in self.context.request.arguments \
		or "wmn" in self.context.request.arguments:
			path = self.context.request.arguments["i"].split("/", 3)[-1]
			self.ceph_s3(self.context.config.CEPH_NORMALIZED_BUCKET, path, bytes)
			self.amazon_s3(self.context.config.AMAZON_NORMALIZED_BUCKET, path, bytes)
		elif len(url.split("/")) >= 2:
			self.ceph_s3(self.context.config.CEPH_CACHE_BUCKET, url, bytes)

	def get(self):
		pass

	def last_updated(self):
		pass
